Summary:        A set of mono-spaced OpenType fonts designed for coding environments
Name:           adobe-source-code-pro-fonts
Version:        2.042.1.062.1.026
Release:        3%{?dist}
License:        OFL
URL:            https://github.com/adobe-fonts/source-code-pro
Source0:        https://github.com/adobe-fonts/source-code-pro/archive/refs/tags/2.042R-u/1.062R-i/1.026R-vf.tar.gz
Source1:        adobe-source-code-pro-fonts-fontconfig.conf
Source2:        adobe-source-code-pro.metainfo.xml

BuildArch:      noarch
BuildRequires:  fontpackages-devel
Requires:       fontpackages-filesystem

%description
This font was designed by Paul D. Hunt as a companion to Source Sans. It has
the same weight range as the corresponding Source Sans design.  It supports
a wide range of languages using the Latin script, and includes all the
characters in the Adobe Latin 4 glyph set.


%prep
%autosetup -n source-code-pro-2.042R-u-1.062R-i-1.026R-vf

%build

%install
install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p OTF/*.otf %{buildroot}%{_fontdir}
install -m 0644 -p VF/*.otf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} %{buildroot}%{_fontconfig_templatedir}/61-adobe-source-code-pro.conf
ln -s %{_fontconfig_templatedir}/61-adobe-source-code-pro.conf \
      %{buildroot}%{_fontconfig_confdir}/61-adobe-source-code-pro.conf

install -Dm 0644 -p %{SOURCE2} \
        %{buildroot}%{_datadir}/appdata/adobe-source-code-pro.metainfo.xml

	
%files  
%license LICENSE.md
%doc README.md
%dir %{_fontbasedir}/adobe-source-code-pro-fonts
%{_datadir}/appdata/adobe-source-code-pro.metainfo.xml
%{_fontdir}/*.otf
%{_fontconfig_confdir}/61-adobe-source-code-pro.conf
%{_fontconfig_templatedir}/61-adobe-source-code-pro.conf


%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.042.1.062.1.026-3
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.042.1.062.1.026-2
- Rebuilt for loongarch release

* Wed Dec 20 2023 Upgrade Robot <upbot@opencloudos.org> - 2.042.1.062.1.026-1
- Upgrade to version 2.042.1.062.1.026

* Wed Sep 13 2023 rockerzhu <rockerzhu@tencent.com> - 2.030.1.050-5
- Rebuilt for fontconfig to privode font(:lang=)

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.030.1.050-4
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.030.1.050-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.030.1.050-2
- Rebuilt for OpenCloudOS Stream 23

* Thu Nov 24 2022 Feng Weiyao <wynnfeng@tencent.com> - 2.030.1.050-1
- package init.
